﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_productos_Online.modelo
{
    class Vendedor
    {
        public string cod_vendedor { get; set; }
        public string nombre_vendedor { get; set; }
        public string link_pagina { get; set; }
    }
}
