﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_productos_Online.modelo
{
    class Producto
    {
        public string cod_producto { get; set; }
        public string nombre_producto { get; set; }
        public string nombre_producto_proveedor { get; set; }
        public string link_proveedor { get; set; }
        public string unidades_disponibles { get; set; }
        public string costo_envio { get; set; }
        public string precio_proveedor { get; set; }
        public string cod_proveedor { get; set; }
        public string nombre_producto_venta { get; set; }
        public string link_venta { get; set; }
        public string precio_venta { get; set; }
        public string cod_vendedor { get; set; }
    }
}
