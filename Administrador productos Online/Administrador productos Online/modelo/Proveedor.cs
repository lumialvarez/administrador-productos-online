﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_productos_Online.modelo
{
    class Proveedor
    {
        public string cod_proveedor { get; set; }
        public string nombre_proveedor { get; set; }
        public string link_pagina { get; set; }
    }
}
