﻿namespace Administrador_productos_Online
{
    partial class frmAdministrarProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tblProductos = new System.Windows.Forms.DataGridView();
            this.cod_producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre_producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precio_proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precio_venta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_vendedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_nombreProducto = new System.Windows.Forms.TextBox();
            this.txt_unidadesDispo = new System.Windows.Forms.TextBox();
            this.txt_costoEnvio = new System.Windows.Forms.TextBox();
            this.txt_costo = new System.Windows.Forms.TextBox();
            this.txt_linkProveedor = new System.Windows.Forms.TextBox();
            this.txt_NombreProductoProveedor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblProductos)).BeginInit();
            this.SuspendLayout();
            // 
            // tblProductos
            // 
            this.tblProductos.AllowUserToAddRows = false;
            this.tblProductos.AllowUserToDeleteRows = false;
            this.tblProductos.AllowUserToResizeColumns = false;
            this.tblProductos.AllowUserToResizeRows = false;
            this.tblProductos.BackgroundColor = System.Drawing.SystemColors.Control;
            this.tblProductos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tblProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cod_producto,
            this.nombre_producto,
            this.precio_proveedor,
            this.cod_proveedor,
            this.precio_venta,
            this.cod_vendedor});
            this.tblProductos.EnableHeadersVisualStyles = false;
            this.tblProductos.Location = new System.Drawing.Point(12, 12);
            this.tblProductos.Name = "tblProductos";
            this.tblProductos.ReadOnly = true;
            this.tblProductos.RowHeadersVisible = false;
            this.tblProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tblProductos.Size = new System.Drawing.Size(523, 371);
            this.tblProductos.TabIndex = 0;
            this.tblProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tblProductos_CellClick);
            // 
            // cod_producto
            // 
            this.cod_producto.HeaderText = "cod_producto";
            this.cod_producto.Name = "cod_producto";
            this.cod_producto.ReadOnly = true;
            this.cod_producto.Visible = false;
            // 
            // nombre_producto
            // 
            this.nombre_producto.HeaderText = "Producto";
            this.nombre_producto.Name = "nombre_producto";
            this.nombre_producto.ReadOnly = true;
            this.nombre_producto.Width = 300;
            // 
            // precio_proveedor
            // 
            this.precio_proveedor.HeaderText = "Val. Compra";
            this.precio_proveedor.Name = "precio_proveedor";
            this.precio_proveedor.ReadOnly = true;
            // 
            // cod_proveedor
            // 
            this.cod_proveedor.HeaderText = "cod_proveedor";
            this.cod_proveedor.Name = "cod_proveedor";
            this.cod_proveedor.ReadOnly = true;
            this.cod_proveedor.Visible = false;
            // 
            // precio_venta
            // 
            this.precio_venta.HeaderText = "Val. venta";
            this.precio_venta.Name = "precio_venta";
            this.precio_venta.ReadOnly = true;
            // 
            // cod_vendedor
            // 
            this.cod_vendedor.HeaderText = "cod_vendedor";
            this.cod_vendedor.Name = "cod_vendedor";
            this.cod_vendedor.ReadOnly = true;
            this.cod_vendedor.Visible = false;
            // 
            // txt_nombreProducto
            // 
            this.txt_nombreProducto.Location = new System.Drawing.Point(525, 41);
            this.txt_nombreProducto.Name = "txt_nombreProducto";
            this.txt_nombreProducto.Size = new System.Drawing.Size(562, 20);
            this.txt_nombreProducto.TabIndex = 1;
            // 
            // txt_unidadesDispo
            // 
            this.txt_unidadesDispo.Location = new System.Drawing.Point(525, 158);
            this.txt_unidadesDispo.Name = "txt_unidadesDispo";
            this.txt_unidadesDispo.Size = new System.Drawing.Size(562, 20);
            this.txt_unidadesDispo.TabIndex = 2;
            // 
            // txt_costoEnvio
            // 
            this.txt_costoEnvio.Location = new System.Drawing.Point(525, 197);
            this.txt_costoEnvio.Name = "txt_costoEnvio";
            this.txt_costoEnvio.Size = new System.Drawing.Size(562, 20);
            this.txt_costoEnvio.TabIndex = 3;
            // 
            // txt_costo
            // 
            this.txt_costo.Location = new System.Drawing.Point(525, 236);
            this.txt_costo.Name = "txt_costo";
            this.txt_costo.Size = new System.Drawing.Size(562, 20);
            this.txt_costo.TabIndex = 4;
            // 
            // txt_linkProveedor
            // 
            this.txt_linkProveedor.Location = new System.Drawing.Point(525, 119);
            this.txt_linkProveedor.Name = "txt_linkProveedor";
            this.txt_linkProveedor.Size = new System.Drawing.Size(562, 20);
            this.txt_linkProveedor.TabIndex = 6;
            // 
            // txt_NombreProductoProveedor
            // 
            this.txt_NombreProductoProveedor.Location = new System.Drawing.Point(525, 80);
            this.txt_NombreProductoProveedor.Name = "txt_NombreProductoProveedor";
            this.txt_NombreProductoProveedor.Size = new System.Drawing.Size(562, 20);
            this.txt_NombreProductoProveedor.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(525, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Producto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(522, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nombre en Pagina Venta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(522, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Link Pagina Venta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(525, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Unidades disponibles";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(525, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Costo envio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(525, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Costo";
            // 
            // frmAdministrarProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 395);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_NombreProductoProveedor);
            this.Controls.Add(this.txt_linkProveedor);
            this.Controls.Add(this.txt_costo);
            this.Controls.Add(this.txt_costoEnvio);
            this.Controls.Add(this.txt_unidadesDispo);
            this.Controls.Add(this.txt_nombreProducto);
            this.Controls.Add(this.tblProductos);
            this.Name = "frmAdministrarProductos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAdministrarProductos";
            this.Load += new System.EventHandler(this.frmAdministrarProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblProductos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView tblProductos;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre_producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio_proveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_proveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio_venta;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_vendedor;
        private System.Windows.Forms.TextBox txt_nombreProducto;
        private System.Windows.Forms.TextBox txt_unidadesDispo;
        private System.Windows.Forms.TextBox txt_costoEnvio;
        private System.Windows.Forms.TextBox txt_costo;
        private System.Windows.Forms.TextBox txt_linkProveedor;
        private System.Windows.Forms.TextBox txt_NombreProductoProveedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}