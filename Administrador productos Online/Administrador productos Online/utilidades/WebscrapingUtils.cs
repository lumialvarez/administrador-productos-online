﻿using Administrador_productos_Online.modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_productos_Online.utilidades
{
    class WebscrapingUtils
    {
        public static Producto obtenerInfoProducto(string urlToLoad)
        {
            Producto producto = new Producto();

            HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.OptionFixNestedTags = true;

            HttpWebRequest request = HttpWebRequest.Create(urlToLoad) as HttpWebRequest;
            request.AllowAutoRedirect = true;
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(new Uri("http://es.aliexpress.com"),
                new Cookie("aep_usuc_f", "region=CO&site=esp&b_locale=es_ES&af_tid=f6a3ff2c9e1b494dac28f653c2b263a1-1459570435077-09197-Nvue6Zja&c_tp=USD")); //Cookie de la pagina
            request.CookieContainer.Add(new Uri("http://es.aliexpress.com"),
                new Cookie("__utmt", "1")); //Cookie de la pagina
            request.CookieContainer.Add(new Uri("http://es.aliexpress.com"),
                new Cookie("_gat", "1")); //Cookie de la pagina
            request.Method = "GET";

            /* Sart browser signature */
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "es_ES,es;q=0.5");
            /* Sart browser signature */

            Console.WriteLine(request.RequestUri.AbsoluteUri);
            WebResponse response = request.GetResponse();

            htmlDoc.Load(response.GetResponseStream(), true);
            if (htmlDoc.DocumentNode != null)
            {
                var nodoPrecio = htmlDoc.DocumentNode.SelectSingleNode("//span[@id='j-sku-price']");
                if (nodoPrecio != null)
                {
                    producto.precio_proveedor = WebUtility.HtmlDecode(nodoPrecio.InnerText.Trim());
                }

                var nodoNombre = htmlDoc.DocumentNode.SelectSingleNode("//h1[@class='product-name']");
                if (nodoNombre != null)
                {
                    producto.nombre_producto_proveedor = WebUtility.HtmlDecode(nodoNombre.InnerText.Trim());
                }

                var nodoCostoEnvio = htmlDoc.DocumentNode.SelectSingleNode("//span[@class='shipping-cost']//b");
                if (nodoCostoEnvio != null)
                {
                    producto.costo_envio = WebUtility.HtmlDecode(nodoCostoEnvio.InnerText.Trim());
                }
                var nodoUnidades = htmlDoc.DocumentNode.SelectSingleNode("//me[@id='j-sell-stock-num']");
                if (nodoUnidades != null)
                {
                    producto.unidades_disponibles = WebUtility.HtmlDecode(nodoUnidades.InnerText.Trim());
                }
            }

            return producto;
        }
    }
}
