﻿using Administrador_productos_Online.utilidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_productos_Online
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void btnAdminProduct_Click(object sender, EventArgs e)
        {
            frmAdministrarProductos frm = new frmAdministrarProductos();
            frm.Show();
        }

    }
}
