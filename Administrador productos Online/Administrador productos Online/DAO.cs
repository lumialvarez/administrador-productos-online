﻿using Administrador_productos_Online.modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administrador_productos_Online
{
    class DAO
    {
        private SQLiteConnection conectar()
        {
            SQLiteConnection ObjConnection = new SQLiteConnection("Data Source=C:\\Users\\KAISER\\Desktop\\pruebaDBsqlite.db;");
            return ObjConnection;
        }

        private DataTable ejecutaConsulta(string sql)
        {
            DataTable dt = new DataTable();
	        try
	        {

                SQLiteConnection cnn = new SQLiteConnection(conectar());
	            cnn.Open();
	            SQLiteCommand mycommand = new SQLiteCommand(cnn);
	            mycommand.CommandText = sql;
	            SQLiteDataReader reader = mycommand.ExecuteReader();
	            dt.Load(reader);
	            reader.Close();
	            cnn.Close();
	        }
	        catch (Exception e)
	        {
	            Console.Write(e.Message);
	        }
	        return dt;
        }


        public List<Producto> cargarProductos()
        {
            
            String sql = "select * from t_producto";
            DataTable data = ejecutaConsulta(sql);
            List<Producto> productos = new List<Producto>();

            foreach (DataRow reader in data.Rows){
                try
                {
                    Producto p = new Producto();
                    p.cod_producto = reader["cod_producto"].ToString();
                    p.nombre_producto = reader["nombre_producto"].ToString();
                    p.nombre_producto_proveedor = reader["nombre_producto_proveedor"].ToString();
                    p.link_proveedor = reader["link_proveedor"].ToString();
                    p.unidades_disponibles = reader["unidades_disponibles"].ToString();
                    p.costo_envio = reader["costo_envio"].ToString();
                    p.precio_proveedor = reader["precio_proveedor"].ToString();
                    p.cod_proveedor = reader["cod_proveedor"].ToString();
                    p.nombre_producto_venta = reader["nombre_producto_venta"].ToString();
                    p.link_venta = reader["link_venta"].ToString();
                    p.precio_venta = reader["precio_venta"].ToString();
                    p.cod_vendedor = reader["cod_vendedor"].ToString();

                    productos.Add(p);
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
            }

            return productos;

        }

        //update t_producto set nombre_producto_proveedor = '', unidades_disponibles = '', costo_envio = '', precio_proveedor = '' where cod_producto = '';
        public void actualizarProducto(Producto producto)
        {
            String sql = "update t_producto set nombre_producto_proveedor = '"+producto.nombre_producto_proveedor+"', unidades_disponibles = '"+producto.unidades_disponibles+"', costo_envio = '"+producto.costo_envio+"', precio_proveedor = '"+producto.precio_proveedor+"' where cod_producto = '"+producto.cod_producto+"';";
            DataTable data = ejecutaConsulta(sql);
        }


    }
}
