﻿using Administrador_productos_Online.modelo;
using Administrador_productos_Online.utilidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Administrador_productos_Online
{
    public partial class frmAdministrarProductos : Form
    {
        DAO dao;
        List<Producto> productos = new List<Producto>();

        public frmAdministrarProductos()
        {
            InitializeComponent();
            dao = new DAO();
        }

        private void frmAdministrarProductos_Load(object sender, EventArgs e)
        {
            frmLoad form = new frmLoad();
            form.Show();
            Application.DoEvents();

            try
            {
                actualizarDatos();
                cargarDatos();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            

            form.Close();
        }

        private void actualizarDatos()
        {
            List<Producto> tmp_productos = dao.cargarProductos();
            if (tmp_productos.Count > 0)
            {
                foreach (Producto p in tmp_productos)
                {
                    Producto tmp = WebscrapingUtils.obtenerInfoProducto(p.link_proveedor);
                    tmp.cod_producto = p.cod_producto;
                    dao.actualizarProducto(tmp);
                }
            }
        }

        private void cargarDatos()
        {
            productos = dao.cargarProductos();

            if (productos.Count > 0)
            {
                tblProductos.Rows.Clear();
                tblProductos.Refresh();
                foreach (Producto p in productos)
                {
                    tblProductos.Rows.Add(new object[] { p.cod_producto, p.nombre_producto, p.precio_proveedor, p.cod_proveedor, p.precio_venta, p.cod_vendedor});
                }
            }
        }

        private void tblProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int pos = e.RowIndex;
                String codigo = tblProductos.Rows[pos].Cells["cod_producto"].Value.ToString();
                foreach (Producto p in productos)
                {
                    if (p.cod_producto.Equals(codigo))
                    {
                        txt_nombreProducto.Text = p.nombre_producto;
                        txt_NombreProductoProveedor.Text = p.nombre_producto_proveedor;
                        txt_linkProveedor.Text = p.link_proveedor;
                        txt_unidadesDispo.Text = p.unidades_disponibles;
                        txt_costoEnvio.Text = p.costo_envio;
                        txt_costo.Text = p.precio_proveedor;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            
        }
    }
}
